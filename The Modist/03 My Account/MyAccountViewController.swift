//
//  MyAccountViewController.swift
//  The Modist
//
//  Created by Eastern Techno Solutions on 21/09/19.
//  Copyright © 2019 Eastern Techno Solutions. All rights reserved.
//

import UIKit
import WebKit
class MyAccountViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let url=URL(string: "http://easternts.com.au");
        webView.load(URLRequest(url: url!));
    }
    

}


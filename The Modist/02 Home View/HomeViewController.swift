//
//  HomeViewController.swift
//  The Modist
//
//  Created by Eastern Techno Solutions on 21/09/19.
//  Copyright © 2019 Eastern Techno Solutions. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import AVKit

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    
    
    @IBOutlet weak var scrMain:UIScrollView!
    
    //MARK:  - Top Banner
    @IBOutlet weak var scrPromo:UIScrollView!
    //MARK:  - Top Carousel
    @IBOutlet weak var scrTopCarousel:UIScrollView!
    @IBOutlet weak var lblCarouselTitle:UILabel!
    @IBOutlet weak var lblCarouselDesc:UILabel!
    @IBOutlet weak var lblLinkTitle:UILabel!
    
    //MARK:  - Ad Banner
    @IBOutlet weak var imgAdBanner:UIImageView!
    
    //MARK:  - Just Landed
    @IBOutlet weak var collectionProduct:UICollectionView?
    
    //MARK:  - Fix Grid 1
    @IBOutlet weak var imgGrid1:UIImageView!
    @IBOutlet weak var lblGrid1Title:UILabel!
    @IBOutlet weak var lblGrid1Discription:UILabel!
    @IBOutlet weak var lblGrid1LinkTitle:UILabel!
    
    //MARK:  - Fix Grid 2
    @IBOutlet weak var imgGrid2:UIImageView!
    @IBOutlet weak var lblGrid2Title:UILabel!
    @IBOutlet weak var lblGrid2Discription:UILabel!
    @IBOutlet weak var lblGrid2LinkTitle:UILabel!
    
    //MARK:  - Fix Grid 3
    @IBOutlet weak var imgGrid3:UIImageView!
    @IBOutlet weak var lblGrid3Title:UILabel!
    @IBOutlet weak var lblGrid3Discription:UILabel!
    @IBOutlet weak var lblGrid3LinkTitle:UILabel!
    
    //MARK:  - Fix Grid 4
    @IBOutlet weak var imgGrid4:UIImageView!
    @IBOutlet weak var lblGrid4Title:UILabel!
    @IBOutlet weak var lblGrid4Discription:UILabel!
    @IBOutlet weak var lblGrid4LinkTitle:UILabel!
    
    //MARK:  - Bottom Banner
    @IBOutlet var imgBottomBanner:UIImageView!
    
    //MARK: - Variable(Object) Declaration
    var dicResult: [String: Any] = [:]
    var arrPromo = [Any]()
    var arrTopCarousel = [Any]()
    var arrAdBanner = [Any]()
    var arrProductCollection = [Any]()
    var arrGridProduct = [Any]()
    var arrBottomBanner = [Any]()
    
    var promoTimer:Timer = Timer()
    var CarouselTimer:Timer = Timer()
    
    var btnPlayButton:UIButton = UIButton()
    let playerViewController = AVPlayerViewController()
    
    // MARK:  - General Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        scrMain.contentSize = CGSize(width: 0, height: 1330)
        self.callWebservice()
        
    }
    // MARK: -  API CALL Methods
    func callWebservice() {
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: "https://dev.themodist.com/") as URL?)
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        let params = [
            "c_preferred_country": "IN",
            "c_preferred_currency": "USD"
        ]
        manager.get("on/demandware.store/Sites-TheModist-Site/en/JSON-HomePage", parameters: params, progress: { (Progress) in
            
        }, success: { (task:URLSessionDataTask, responseObject:Any?) in
            print(responseObject!)
            
            guard let dict = responseObject as? [AnyHashable: Any] else {
                
                return
            }
            
            self.dicResult = (dict["default"] as? [String: Any])!
            
            self.arrPromo = (self.dicResult["promo_strip"] as? Array<Any>)!
            print(self.arrPromo)
            self.arrTopCarousel = (self.dicResult["carousel_content"] as? Array<Any>)!
            print(self.arrTopCarousel)
            self.arrAdBanner = (self.dicResult["banner_top"] as? Array<Any>)!
            print(self.arrAdBanner)
            let tempDic = self.dicResult["carousel_product"] as? [String: Any]
            print(tempDic!)
            self.arrProductCollection = (tempDic!["products"] as? Array<Any>)!
            print(self.arrProductCollection)
            self.arrGridProduct = (self.dicResult["grid_1"] as? Array<Any>)!
            print(self.arrGridProduct)
            if self.dicResult.keys.contains("banner_bottom") {
                self.arrBottomBanner = (self.dicResult["banner_bottom"] as? Array<Any>)!
                print(self.arrBottomBanner)
                self.setUpBottomStrip()
            }
            
            
            
            self.setUpPromoStrip()
            self.setUpTopCarousel()
            self.setUpAdBanner()
            self.collectionProduct?.reloadData()
            self.setUpGrid()
            
            
            
        }) { (task: URLSessionDataTask?, error: Error) in
            print(error)
        }
    }
    // MARK: - UICollectionViewDataSource protocol
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrProductCollection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath as IndexPath) as! ProductCell
        
        
        let tempDic = arrProductCollection[indexPath.row] as? [String: Any]
        let tempDic1 = tempDic!["c_image"] as? [String: Any]
        
        let imageUrl = NSURL(string: (tempDic1!["link"] as? String)!)
        
        cell.imgView?.sd_setImage(with: imageUrl! as URL, completed: { (image:UIImage?, error:Error?, type:SDImageCacheType, url:URL?) in
            cell.imgView?.image = image
        })
        cell.lblTitle?.text = tempDic!["c_brand"] as? String
        
        
        return cell
    }
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    
    //MARK: - Other UI Methods
    func setUpPromoStrip() {
        let screenSize = UIScreen.main.bounds
        let screenWidth:Int = Int(screenSize.width)
        
        for i in 0..<arrPromo.count {
            
            let dicTemp = arrPromo[i] as? [String: Any]
            let dicTemp1 = dicTemp!["message"] as? [String: Any]
            
            let lblPromoText = UILabel.init(frame: CGRect(x: i * screenWidth, y: 0, width: screenWidth, height: 35))
            lblPromoText.text = dicTemp1!["link_title"] as? String
            lblPromoText.textAlignment = NSTextAlignment.center
            lblPromoText.backgroundColor = UIColor.black
            lblPromoText.textColor = UIColor.white
            scrPromo.addSubview(lblPromoText)
        }
        scrPromo.contentSize = CGSize(width: screenWidth * arrPromo.count, height: 35)
        
        promoTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(topBannerTimer), userInfo: nil, repeats: true)
    }
    func setUpTopCarousel() {
        let screenSize = UIScreen.main.bounds
        let screenWidth:Int = Int(screenSize.width)
        
        for i in 0..<arrTopCarousel.count {
            
            let dicTemp = arrTopCarousel[i] as? [String: Any]
            let dicTemp1 = dicTemp!["slideDescr"] as? [String: Any]
            
            let imgMainBanner = UIImageView.init(frame: CGRect(x: i * screenWidth, y: 0, width: screenWidth, height: 210))
            
            let stringURL = dicTemp!["img_src"] as! String
            
            if stringURL.count == 0 {
                let dicTemp2 = dicTemp!["video"] as? [String: Any]
                let stringVideoURL = dicTemp2!["video_poster"] as! String
                let escapeURL = stringVideoURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let imageUrl = NSURL(string: escapeURL!)
                
                imgMainBanner.sd_setImage(with: imageUrl! as URL, completed: { (image:UIImage?, error:Error?, type:SDImageCacheType, url:URL?) in
                    imgMainBanner.image = image
                    imgMainBanner.contentMode = UIView.ContentMode.scaleToFill
                })
                scrTopCarousel.addSubview(imgMainBanner)
                
                btnPlayButton = UIButton.init(frame: CGRect(x: (i * screenWidth) + (screenWidth / 2) - 15, y: 90, width: 30, height: 30))
                btnPlayButton.addTarget(self, action: #selector(self.btnPlayPressed), for: .touchUpInside)
                btnPlayButton.setImage(UIImage.init(named: "btnPlay"), for: .normal)
                scrTopCarousel.addSubview(btnPlayButton)
                
            }
            else {
                let escapeURL = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let imageUrl = NSURL(string: escapeURL!)
                
                imgMainBanner.sd_setImage(with: imageUrl! as URL, completed: { (image:UIImage?, error:Error?, type:SDImageCacheType, url:URL?) in
                    imgMainBanner.image = image
                    imgMainBanner.contentMode = UIView.ContentMode.scaleToFill
                })
                scrTopCarousel.addSubview(imgMainBanner)
            }
            lblCarouselTitle.text = dicTemp!["img_title"] as? String
            lblCarouselDesc.text = dicTemp1!["text"] as? String
            lblLinkTitle.text = dicTemp!["link_title"] as? String
        }
        scrTopCarousel.contentSize = CGSize(width: screenWidth * arrTopCarousel.count, height: 210)
        CarouselTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(topCarouselTimer), userInfo: nil, repeats: true)
    }
    func setUpAdBanner() {
        if arrAdBanner.count > 0 {
            let dicTemp = arrAdBanner[0] as? [String: Any]
            
            let stringURL = dicTemp!["img_src"] as! String
            let escapeURL = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let imageUrl = NSURL(string: escapeURL!)
            
            imgAdBanner.sd_setImage(with: imageUrl! as URL, completed: { (image:UIImage?, error:Error?, type:SDImageCacheType, url:URL?) in
                self.imgAdBanner.image = image
                self.imgAdBanner.contentMode = UIView.ContentMode.scaleToFill
            })
        }
    }
    func setUpGrid() {
        for i in 0..<arrGridProduct.count {
            let tempDic = arrGridProduct[i] as? [String: Any]
            if i == 0 {
                
                let stringURL = tempDic!["img_src"] as! String
                let escapeURL = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let imageUrl = NSURL(string: escapeURL!)
                
                imgGrid1.sd_setImage(with: imageUrl! as URL, completed: { (image:UIImage?, error:Error?, type:SDImageCacheType, url:URL?) in
                    self.imgGrid1.image = image
                    self.imgGrid1.contentMode = UIView.ContentMode.scaleToFill
                })
                lblGrid1Title.text = (tempDic!["img_title"] as? String)!
                lblGrid1Title.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid1Title.backgroundColor = UIColor.clear
                
                if tempDic?["slideSubtitle"] is NSNull {
                    lblGrid1Discription.text = ""
                }
                else {
                    lblGrid1Discription.text = (tempDic!["slideSubtitle"] as? String)!
                }
                
                lblGrid1Discription.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid1Discription.backgroundColor = UIColor.clear
                
                lblGrid1LinkTitle.text = (tempDic!["link_title"] as? String)!
                lblGrid1LinkTitle.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid1LinkTitle.backgroundColor = UIColor.clear
                
            }
            else if i == 1 {
                let stringURL = tempDic!["img_src"] as! String
                let escapeURL = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let imageUrl = NSURL(string: escapeURL!)
                
                
                imgGrid2.sd_setImage(with: imageUrl! as URL, completed: { (image:UIImage?, error:Error?, type:SDImageCacheType, url:URL?) in
                    self.imgGrid2.image = image
                    self.imgGrid2.contentMode = UIView.ContentMode.scaleToFill
                })
                lblGrid2Title.text = (tempDic!["img_title"] as? String)!
                lblGrid2Title.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid2Title.backgroundColor = UIColor.clear
                
                if tempDic?["slideSubtitle"] is NSNull {
                    lblGrid2Discription.text = ""
                }
                else {
                    lblGrid2Discription.text = (tempDic!["slideSubtitle"] as? String)!
                }
                lblGrid2Discription.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid2Discription.backgroundColor = UIColor.clear
                
                lblGrid2LinkTitle.text = (tempDic!["link_title"] as? String)!
                lblGrid2LinkTitle.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid2LinkTitle.backgroundColor = UIColor.clear
                
            }
            else if i == 2 {
                let stringURL = tempDic!["img_src"] as! String
                let escapeURL = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let imageUrl = NSURL(string: escapeURL!)
                
                imgGrid3.sd_setImage(with: imageUrl! as URL, completed: { (image:UIImage?, error:Error?, type:SDImageCacheType, url:URL?) in
                    self.imgGrid3.image = image
                    self.imgGrid3.contentMode = UIView.ContentMode.scaleToFill
                })
                lblGrid3Title.text = (tempDic!["img_title"] as? String)!
                lblGrid3Title.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid3Title.backgroundColor = UIColor.clear
                
                if tempDic?["slideSubtitle"] is NSNull {
                    lblGrid3Discription.text = ""
                }
                else {
                    lblGrid3Discription.text = (tempDic!["slideSubtitle"] as? String)!
                }
                lblGrid3Discription.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid3Discription.backgroundColor = UIColor.clear
                
                lblGrid3LinkTitle.text = (tempDic!["link_title"] as? String)!
                lblGrid3LinkTitle.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid3LinkTitle.backgroundColor = UIColor.clear
                
            }
            else if i == 3 {
                let stringURL = tempDic!["img_src"] as! String
                let escapeURL = stringURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let imageUrl = NSURL(string: escapeURL!)
                
                imgGrid4.sd_setImage(with: imageUrl! as URL, completed: { (image:UIImage?, error:Error?, type:SDImageCacheType, url:URL?) in
                    self.imgGrid4.image = image
                    self.imgGrid4.contentMode = UIView.ContentMode.scaleToFill
                })
                lblGrid4Title.text = (tempDic!["img_title"] as? String)!
                lblGrid4Title.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid4Title.backgroundColor = UIColor.clear
                
                if tempDic?["slideSubtitle"] is NSNull {
                    lblGrid4Discription.text = ""
                }
                else {
                    lblGrid4Discription.text = (tempDic!["slideSubtitle"] as? String)!
                }
                lblGrid4Discription.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid4Discription.backgroundColor = UIColor.clear
                
                lblGrid4LinkTitle.text = (tempDic!["link_title"] as? String)!
                lblGrid4LinkTitle.textColor = self.hexTOColor(hexColor: (tempDic!["text_color"] as? String)!)
                lblGrid4LinkTitle.backgroundColor = UIColor.clear
                
            }
            
            
            
        }
        
        
    }
    func setUpBottomStrip() {
        
        if arrBottomBanner.count > 0 {
            let dicTemp = arrBottomBanner[0] as? [String: Any]
            
            let gifURL : String = dicTemp!["img_src"] as! String
            let imageURL = UIImage.gifImageWithURL(gifURL)
            imgBottomBanner.image = imageURL
        }
    }
    
    //MARK: - UIScrollView Delegate Methods
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool) {
        
        if scrollView == scrTopCarousel {
            CarouselTimer.invalidate()
            CarouselTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(topCarouselTimer), userInfo: nil, repeats: true)
            playerViewController.view.removeFromSuperview()
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView)  {
    
        if scrollView == scrTopCarousel {
            let screenSize = UIScreen.main.bounds
            let screenWidth:Int = Int(screenSize.width)
            let scrolloffset:Int = Int(scrTopCarousel.contentOffset.x)
            let scrViewIndex = scrolloffset / screenWidth
            
            let dicTemp = arrTopCarousel[scrViewIndex] as? [String: Any]
            let dicTemp1 = dicTemp!["slideDescr"] as? [String: Any]
            
            lblCarouselTitle.text = dicTemp!["img_title"] as? String
            lblCarouselDesc.text = dicTemp1!["text"] as? String
            lblLinkTitle.text = dicTemp!["link_title"] as? String
        }
    }
    
    
    //MARK: - Other Methods
    func hexTOColor(hexColor:String) -> UIColor {
        let r, g, b: CGFloat
        
        if hexColor.count == 6 {
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                b = CGFloat((hexNumber & 0x0000ff) >> 0) / 255
                
                return UIColor(red: r, green: g, blue: b, alpha: 1.0)
                
            }
        }
        return UIColor.white
    }
    @objc func topBannerTimer() {
        let screenSize = UIScreen.main.bounds
        let screenWidth:Int = Int(screenSize.width)
        let scrolloffset:Int = Int(scrPromo.contentOffset.x)

        if scrolloffset >= screenWidth * (self.arrPromo.count-1) {
            scrPromo.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        else {
            scrPromo.setContentOffset(CGPoint(x: scrolloffset + screenWidth, y: 0), animated: true)
        }
    }
    @objc func topCarouselTimer() {
        let screenSize = UIScreen.main.bounds
        let screenWidth:Int = Int(screenSize.width)
        let scrolloffset:Int = Int(scrTopCarousel.contentOffset.x)
        let scrViewIndex = scrolloffset / screenWidth
        
        
        if scrolloffset >= screenWidth * (self.arrTopCarousel.count-1) {
            scrTopCarousel.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        }
        else {
            scrTopCarousel.setContentOffset(CGPoint(x: scrolloffset + screenWidth, y: 0), animated: false)
        }
    }
    @objc func btnPlayPressed() {
        CarouselTimer.invalidate()
        
        let screenSize = UIScreen.main.bounds
        let screenWidth:Int = Int(screenSize.width)
        let scrolloffset:Int = Int(scrTopCarousel.contentOffset.x)
        let scrViewIndex = scrolloffset / screenWidth
        
        let dicTemp = arrTopCarousel[scrViewIndex] as? [String: Any]
        let dicTemp2 = dicTemp!["video"] as? [String: Any]
        
        
        
        
        
        let videoURL = URL(string: dicTemp2!["source"] as! String)
        let player = AVPlayer(url: videoURL!)
        if dicTemp2!["muted"] as! Bool {
            player.volume = 0.0;
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
        playerViewController.showsPlaybackControls = false
        playerViewController.player = player
        playerViewController.view.frame = CGRect(x: scrViewIndex * screenWidth, y: 0, width: screenWidth, height: 210)
        playerViewController.player?.play()
        scrTopCarousel.addSubview(playerViewController.view)
    }
    @objc func playerDidFinishPlaying(note: NSNotification) {
        CarouselTimer.invalidate()
        CarouselTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(topCarouselTimer), userInfo: nil, repeats: true)
        playerViewController.view.removeFromSuperview()
    }
}


